package com.kazm.scala.learn

/**
 * Created by Mateusz on 29.06.2016.
 */
object TraitTest extends App {

  for(season <- List("fall", "winter", "spring"))
    println(season + ": " + ChecksumAccumulator.calculate(season))

}
