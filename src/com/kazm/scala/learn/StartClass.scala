package com.kazm.scala.learn

/**
 * Created by Mateusz on 29.06.2016.
 */
object StartClass {

  def main(args: Array[String]): Unit = {
    //TestClass.run()

    //println(ChecksumAccumulator.calculate("elo"))
    //println(ChecksumAccumulator.calculate("cos"))

    println("elo")

    val cos = 0
    val txt = if(cos>5) "elo" else "lol"
    println(txt)

    def greet() = {println("elo")}

    for(number <- 1 to 4) println (s"value $number")


    val filesHere = (new java.io.File(".")).listFiles
    val files = for {file <- filesHere
      if file.getName.contains("a")} yield file.getName
      //println(file)

    val noweCos = "cos"

    println(files.mkString(", "))

    noweCos match {
      case "cos" => println("to jest cos")
      case _ => println("nie wiadomo co to")
    }


    val inc = (x: Int) => x + 1

    val list = 1 :: 2 :: 3 :: Nil

    val result = list.map(x => inc(x)).filter(x => x % 2 == 0)
    println(result.mkString(","))
    val result2 = list.map(inc).filter(_ % 2 == 0)
    println(result2.mkString(","))

    def sum(a:Int, b:Int, c:Int) = a + b + c
    
    //partialy applyed function
    val partSum = sum(1, _:Int, 3)

    val res = partSum(5)
    println(res)

    echo("Siema", "edziu")
    val array = Array("1", "2", "3")
    echo(array: _*) // pass as separate argument

    def boom(x: Int): Int =
      if (x == 0) throw new Exception("boom!")
      else boom(x - 1) + 1

    boom(3)

  }

  def echo(args: String*) = for (arg <- args) println(arg)



}
