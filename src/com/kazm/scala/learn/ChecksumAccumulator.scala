package com.kazm.scala.learn

import scala.collection.mutable

/**
 * Created by Mateusz on 29.06.2016.
 */
class ChecksumAccumulator {

  private var sum = 0

  def add(b: Byte): Unit = sum += b

  def checksum(): Int = ~(sum & 0xFF) + 1

}

object ChecksumAccumulator {

  private val cache = mutable.Map.empty[String, Int]

  def calculate(s: String): Int =
  if (cache.contains(s))
    cache(s)
  else{
    val acc = new ChecksumAccumulator
    for (c <-s)
      acc.add(c.toByte)
    val cs = acc.checksum()
    cache += (s -> cs)
    cs
  }
}