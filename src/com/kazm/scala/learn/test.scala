package com.kazm.scala.learn

import scala.collection.mutable
import scala.io.Source

/**
 * Created by Mateusz on 26.06.2016.
 */

object TestClass {

  def max(x: Int, y: Int): Int = {
    if (x > y) x
    else y
  }

  def plainFunction() = {
    println("elo")
  } //Unit

  def run() = {
    val result = max(2, 3)
    println(result)
    plainFunction()

    var array = Array("1", "2", "3")

    array.foreach((arg: String) => println(arg))
    array.foreach(println(_))
    array.foreach(println) // println takes 1 param, short representation

    for (arg <- array) println(arg)
    // old foreach

    val array2 = new Array[String](3)

    array2(0) = "1"
    array2(1) = "2"
    array2(2) = "3"

    for (arg <- array2) println(arg)

    class SpecialObj {
      def sp(x: String): String = {
        "special: " + x
      }
    }


    val spObj = new SpecialObj()

    println(spObj sp "elo")
    //if function takes 1 arg it can omit dots and brackets


    /////////Lists

    val oneTwoThree = List(1, 2, 3)

    oneTwoThree.foreach(println)

    val fourFive = List(4, 5)
    val resultList = oneTwoThree ::: fourFive

    resultList.foreach((arg: Int) => print(arg + ","))
    val zeroResultList = 0 :: resultList
    println()

    println(zeroResultList.mkString(","))

    val testList = 1 :: 2 :: 3 :: Nil
    println(testList.mkString(","))

    ///Tuple

    val pair = (1, "one")

    println(pair._1)
    println(pair._2)

    //Set

    var jetSet = Set("Airbus", "Boeing")
    println(jetSet.mkString(","))
    jetSet += "Lear"
    println(jetSet.mkString(","))

    jetSet = Set("nowy")
    println(jetSet.mkString(","))

    //Map
    val testMap = mutable.Map[Int, String]()
    testMap += (1 -> "one")
    testMap += (2 -> "two")
    println(testMap.mkString(","))

    val immutableMap = Map(1 -> "one", 2 -> "two")
    println(immutableMap)

    //File short example
    val fileName = "C:/DATA/testFile.txt"

    val lines = Source.fromFile(fileName).getLines().toList
    for (line <- lines) {
      println(line.length + " " + line)
    }
    println("/////////////////////")

    def widthOfLength(s: String) = s.length.toString.length

    var maxWidth = 0
    for (line <- lines) {
      //imperative way
      maxWidth = maxWidth.max(widthOfLength(line))
    }

    val longestLine = lines.reduceLeft(// functional way
      (a, b) => if (a.length > b.length) a else b
    )

    maxWidth = widthOfLength(longestLine)

    for (line <- lines) {
      val numSpaces = maxWidth - widthOfLength(line)
      val padding = " " * numSpaces
      println(padding + line.length + "|" + line)
    }

  }
}